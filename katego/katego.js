var timeToRoll = true;
var turn = 1;

function buttonClick(number) {
    var textbox = document.getElementById("user"+number);
    var status = document.getElementById("status");
    if (!timeToRoll && textbox.value == "") {
	var myRegexp = /= (\d+)/;
	var dieRoll = myRegexp.exec(status.innerHTML);
	textbox.value = dieRoll[1];

	computerMove();
    }
    else {
	status.innerHTML += "!";
    }

}

function computerMove() {
    var die1 = Math.floor(Math.random()*6)+1;
    var die2 = Math.floor(Math.random()*6)+1;
    var status = document.getElementById("status");
    status.innerHTML = "Computer: " + die1 + " + " + die2 + " = " + (die1+die2);
    
    // Place move.  Hacky!
    // Pick a random spot (1-5).  While it is not empty, pick again.
    var position = Math.trunc(Math.random() * 5) + 1;
    var contents = document.getElementById("computer"+position);
    while (contents.value != "") {
	position = Math.trunc(Math.random() * 5) + 1;
	contents = document.getElementById("computer"+position);
    }
    
    contents.value = (die1+die2);

    // Give control back to the player.
    timeToRoll = true;
    turn++;
    if (turn == 6) {
	gameOver();
    }
}

function gameOver() {
    var userPoints = 0;
    var computerPoints = 0;
    for (i=1; i<=5; i++) {
	var user = parseInt(document.getElementById("user"+i).value);
	var computer = parseInt(document.getElementById("computer"+i).value);
	if (user > computer) {
	    userPoints += i;
	}
	else if (computer > user) {
	    computerPoints += i;
	} // No points if a tie.
    }

    var status = document.getElementById("status");
    if (userPoints == computerPoints) {
        status.innerHTML = "Tie game: " + userPoints + "-" + computerPoints;
    }
    else if (userPoints > computerPoints) {
	status.innerHTML = "You win: " + userPoints + "-" + computerPoints;
    }
    else {
	status.innerHTML = "Computer wins: " + userPoints + "-" + computerPoints;
    }
    timeToRoll = false;
}

function roll() {
    var status = document.getElementById("status");
    if (timeToRoll) {
	var die1 = Math.floor(Math.random()*6)+1;
	var die2 = Math.floor(Math.random()*6)+1;
	status.innerHTML = die1 + " + " + die2 + " = " + (die1+die2);
	timeToRoll = false;
    }
    else {
	status.innerHTML += "!";
    }
}

/* window.onload = setup();*/
